<?php
/**
 * Select sanitization callback example.
 *
 * - Sanitization: select
 * - Control: select, radio
 * 
 * Sanitization callback for 'select' and 'radio' type controls. This callback sanitizes `$input`
 * as a slug, and then validates `$input` against the choices defined for the control.
 * 
 * @see sanitize_key()               https://developer.wordpress.org/reference/functions/sanitize_key/
 * @see $wp_customize->get_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/get_control/
 *
 * @param string               $input   Slug to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
 */
if( !function_exists('insurance_lite_sanitize_select')){
	function insurance_lite_sanitize_select( $input, $setting ) {
		
		// Ensure input is a slug.
		$input = sanitize_key( $input );
		
		// Get list of choices from the control associated with the setting.
		$choices = $setting->manager->get_control( $setting->id )->choices;
		
		// If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
	}
}

/**
 * Switch Class sanitization callback example.
 *
 * - Sanitization: switch
 * - Control: switch (manual class)
 * 
 * Sanitization callback for 'switch' type controls. This callback sanitizes `$input`
 * as a slug, and then validates `$input` against the choices defined for the control.
 * 
 * @see sanitize_key()               https://developer.wordpress.org/reference/functions/sanitize_key/
 * @see $wp_customize->get_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/get_control/
 *
 * @param string               $input   Slug to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
 */
if( !function_exists('insurance_lite_sanitize_switch')){
	function insurance_lite_sanitize_switch( $input, $setting ) {
		// Ensure input is a slug.
		$input = sanitize_key( $input );
		
		// Get list of switch_label from the control associated with the setting.
		$choices = $setting->manager->get_control( $setting->id )->switch_label;
		
		// If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
	}
}

/** modify customizer */
if ( ! function_exists( 'insurance_lite_child_options' ) ) {

    function insurance_lite_child_options( $wp_customize ) {
		
		$wp_customize->get_control('construction_light_header_layout')->choices = array(
			'layout_one' => esc_html__('Layout One', 'insurance-lite'),
			'layout_two' => esc_html__('Layout Two', 'insurance-lite'),
			'layout_three' => esc_html__('Layout Three', 'insurance-lite'),
			'layout_agency' => esc_html__('Layout Four', 'insurance-lite'),
		);
		

		
		$wp_customize->get_control('construction_light_service_layout')->choices = array(
			'layout_one'  => esc_html__('Layout One', 'insurance-lite'),
			'layout_two'  =>esc_html__('Layout Two', 'insurance-lite'),
			'layout_three'  =>esc_html__('Layout Three', 'insurance-lite'),
			'layout_four'  =>esc_html__('Layout Four', 'insurance-lite'),
			'layout_five'  =>esc_html__('Layout Five', 'insurance-lite'),
		);
		$wp_customize->get_control('construction_light_service_service_section')->priority = 4;
		$wp_customize->get_control('construction_light_service_layout')->priority = 5;


		$wp_customize->get_control('construction_light_team_layout')->choices = array(
			'layout_one' => esc_html__('Layout One', 'insurance-lite'),
			'layout_two' => esc_html__('Layout Two', 'insurance-lite'),
			'layout_three' => esc_html__('Layout Three', 'insurance-lite'),
		);

		$wp_customize->get_setting('construction_light_primary_color')->default = apply_filters('construction_light_primary_color', '#0162CA');
		$wp_customize->get_setting('header_textcolor')->default = apply_filters('header_textcolor', '#0162CA');
		
		/**
		 * About Us Section additional field
		 */
		$wp_customize->add_setting( 'construction_light_aboutus_layout', array(
			'sanitize_callback' => 'insurance_lite_sanitize_select', 	 //done	
			'default' => 'layout-two'
			// 'transport' => 'postMessage'
		));
		$wp_customize->add_control('construction_light_aboutus_layout', array(
			'label'		=> esc_html__( 'Layout', 'insurance-lite' ),
			'section'	=> 'construction_light_aboutus_section',
			'type'      => 'select',
			'choices' => array(
				'layout-one' => esc_html__('Layout One', 'insurance-lite'),
				'layout-two' => esc_html__('Layout Two', 'insurance-lite'),
			),
			'priority' => -1
		));

		$wp_customize->add_setting( 'construction_light_aboutus_profile_name', array(
			'sanitize_callback' => 'sanitize_text_field', 	 //done	
			// 'transport' => 'postMessage'
		));

		$wp_customize->add_control('construction_light_aboutus_profile_name', array(
			'label'		=> esc_html__( 'Profile Name', 'insurance-lite' ),
			'section'	=> 'construction_light_aboutus_section',
			'type'      => 'text',
			'priority' => 10
		));
		
		$wp_customize->add_setting( 'construction_light_aboutus_profile_role', array(
			'sanitize_callback' => 'sanitize_text_field', 	 //done	
			// 'transport' => 'postMessage'
		));

		$wp_customize->add_control('construction_light_aboutus_profile_role', array(
			'label'		=> esc_html__( 'Designadtion', 'insurance-lite' ),
			'section'	=> 'construction_light_aboutus_section',
			'type'      => 'text',
			'priority' => 10
		));
		$wp_customize->add_setting('construction_light_aboutus_signature', array(
			'transport' => 'postMessage',
			'priority' => 10,
			'sanitize_callback'	=> 'esc_url_raw'		//done
		));

		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'construction_light_aboutus_signature', array(
			'label'	   => esc_html__('Signature Image','insurance-lite'),
			'section'  => 'construction_light_aboutus_section',
		)));


		$wp_customize->get_control('construction_light_aboutus_progressbar')->label = esc_html__( 'Show Info', 'insurance-lite' );
		$wp_customize->add_setting('construction_light_progressbar', array(
		    'sanitize_callback' => 'construction_light_sanitize_repeater',		//done
			'transport' => 'postMessage',
			'default' => json_encode(array(
		        array(
					'icon' => '',
		            'progressbar_title'  =>'',
		            'progressbar_number'  =>'',	            
					'description' => '',
					'number_suffix' => '',

		        )
		    ))
		));

		$wp_customize->add_control(new Construction_Light_Repeater_Control($wp_customize, 
			'construction_light_progressbar', 
			array(
			    'label' 	   => esc_html__('Achievement', 'insurance-lite'),
			    'section' 	   => 'construction_light_aboutus_section',
			    'settings' 	   => 'construction_light_progressbar',
			    'cl_box_label' => esc_html__('Achievement', 'insurance-lite'),
			    'cl_box_add_control' => esc_html__('Add New', 'insurance-lite'),
			    'active_callback' => 'construction_light_active_progressbar'
			),
		    array(
				'icon' => array(
		            'type' => 'icons',
		            'label' => esc_html__('Icon', 'insurance-lite'),
		            'default' => ''
		        ),

		        'progressbar_title' => array(
		            'type' => 'text',
		            'label' => esc_html__('Title', 'insurance-lite'),
		            'default' => ''
		        ),

				'description' => array(
		            'type' => 'textarea',
		            'label' => esc_html__('Description', 'insurance-lite'),
		            'default' => ''
		        ),
		        'progressbar_number' => array(
		            'type' => 'number',
		            'label' => esc_html__('Value ', 'insurance-lite'),
		            'default' => '',
					'min' => 1,
					'max' => 100
		        ),
				'number_suffix' => array(
		            'type' => 'text',
		            'label' => esc_html__('Number Suffix', 'insurance-lite'),
		            'default' => '%',
		        ),
			)
		));

		/**
		 * about us award layout
		 */
		$wp_customize->add_setting( 'construction_light_aboutus_award_column', array(
			'sanitize_callback' => 'insurance_lite_sanitize_select', 	 //done	
			'default' => '2'
			// 'transport' => 'postMessage'
		));
		$wp_customize->add_control('construction_light_aboutus_award_column', array(
			'label'		=> esc_html__( 'Award Column', 'insurance-lite' ),
			'section'	=> 'construction_light_aboutus_section',
			'type'      => 'select',
			'choices' => array(
				'1' => esc_html__('1', 'insurance-lite'),
				'2' => esc_html__('2', 'insurance-lite'),
				'3' => esc_html__('3', 'insurance-lite'),
				'4' => esc_html__('4', 'insurance-lite'),
			),
			'priority' => 40
		));



		/** contact section */
		$wp_customize->add_section('construction_light_contact_section', array(
			'title' => esc_html__('Contact Section', 'insurance-lite'),
			'panel' => 'construction_light_frontpage_settings',
			'priority' => construction_light_get_section_position('construction_light_contact_section') or 100,
			'hiding_control' => 'construction_light_contact_section_disable'
		));

		//ENABLE/DISABLE SERVICE SECTION
		$wp_customize->add_setting('construction_light_contact_section_disable', array(
			'sanitize_callback' => 'insurance_lite_sanitize_switch',
			'transport' => 'postMessage',
			'default' => 'disable'
		));

		$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'construction_light_contact_section_disable', array(
			'section' => 'construction_light_contact_section',
			'label' => esc_html__('Enable Section ', 'insurance-lite'),
			'switch_label' => array(
				'enable' => esc_html__('Yes', 'insurance-lite'),
				'disable' => esc_html__('No', 'insurance-lite'),
			),
			'class' => 'switch-section',
			'priority' => -1
		)));

		// Section Title.
		$wp_customize->add_setting( 'construction_light_contact_title', array(
			'sanitize_callback' => 'sanitize_text_field', 	 //done	
			'transport' => 'postMessage'
		));

		$wp_customize->add_control('construction_light_contact_title', array(
			'label'		=> esc_html__( 'Enter Section Title', 'insurance-lite' ),
			'section'	=> 'construction_light_contact_section',
			'type'      => 'text'
		));

		//Section Sub Title.
		$wp_customize->add_setting( 'construction_light_contact_sub_title', array(
			'sanitize_callback' => 'sanitize_text_field',			//done
			'transport' => 'postMessage'
		) );

		$wp_customize->add_control( 'construction_light_contact_sub_title', array(
			'label'    => esc_html__( 'Enter Section Sub Title', 'insurance-lite' ),
			'section'  => 'construction_light_contact_section',
			'type'     => 'text',
		));

		$wp_customize->add_setting('construction_light_contact_quick_link', array(
			'sanitize_callback' => 'sanitize_text_field'
		));
		$wp_customize->add_control(new InsuranceLiteInfoText($wp_customize, 'construction_light_contact_quick_link', array(
			'label' => esc_html__('Contact Info', 'insurance-lite'),
			'section' => 'construction_light_contact_section',
			'description' => sprintf(esc_html__('Add your %s here, content is comes from top header quick info', 'insurance-lite'), '<a href="?autofocus[section]=construction_light_top_header" target="_blank">Contact Info</a>')
		)));

		$wp_customize->add_setting( 'construction_light_contact_shortcode', array(
			'sanitize_callback' => 'construction_light_sanitize_text', 	 //done	
			'transport' => 'postMessage'
		));

		$wp_customize->add_control('construction_light_contact_shortcode', array(
			'label'		=> esc_html__( 'Contact Form Shortcode', 'insurance-lite' ),
			'section'	=> 'construction_light_contact_section',
			'type'      => 'text'
		));

		$wp_customize->add_setting( 'construction_light_contact_map', array(
			'sanitize_callback' => 'construction_light_sanitize_text', 	 //done	
			'transport' => 'postMessage'
		));

		$wp_customize->add_control('construction_light_contact_map', array(
			'label'		=> esc_html__( 'Enter Map Iframe', 'insurance-lite' ),
			'section'	=> 'construction_light_contact_section',
			'type'      => 'textarea'
		));

		$wp_customize->selective_refresh->add_partial('construction_light_contact_title', array(
			'selector' => '#cl-contact-section .section-title',
			'container_inclusive' => true
		));

		$wp_customize->selective_refresh->add_partial('construction_light_contact_shortcode', array(
			'selector' => '#cl-contact-section .contact-and-map-section',
			'container_inclusive' => true
		));

		$wp_customize->selective_refresh->add_partial( 'construction_light_contact_refresh', array (
			'settings' => array( 
				'construction_light_contact_section_disable',
				'construction_light_contact_title',
				'construction_light_contact_sub_title',
				'construction_light_contact_shortcode',
				'construction_light_contact_map',
		
			),
			'selector' => '#cl-contact-section',
			'fallback_refresh' => false,
			'container_inclusive' => true,
			'render_callback' => function () {
				return get_template_part( 'section/section-contact' );
			}
		));




        /** Features Service Section */
        $wp_customize->get_control('construction_light_features_service_section')->priority = -1;
        $wp_customize->add_setting( 'construction_light_features_service_title', array(
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field'			//done
		) );

		$wp_customize->add_control( 'construction_light_features_service_title', array(
			'label'    => esc_html__( 'Section Title', 'insurance-lite' ),
			'section'  => 'construction_light_promoservice_section',
			'type'     => 'text',
            'priority' => 1,
		));

		$wp_customize->selective_refresh->add_partial('construction_light_features_service_title', array(
			'settings' => array('construction_light_features_service_title'),
			'selector' => '#cl-service-section .section-title',
		));

		
		$wp_customize->add_setting( 'construction_light_features_service_sub_title', array(
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',			//done
            
		));

		$wp_customize->add_control( 'construction_light_features_service_sub_title', array(
			'label'    => esc_html__( 'Sub Title', 'insurance-lite' ),
			'section'  => 'construction_light_promoservice_section',
			'type'     => 'text',
            'priority' => 2,
		));

        $wp_customize->add_setting( 'construction_light_promoservice_style', array(
			'sanitize_callback' => 'insurance_lite_sanitize_select', 	 //done	
			'default' => 'layout_four'
			// 'transport' => 'postMessage'
		));
		$wp_customize->add_control('construction_light_promoservice_style', array(
			'label'		=> esc_html__( 'Layout', 'insurance-lite' ),
			'section'	=> 'construction_light_promoservice_section',
			'type'      => 'select',
			'choices' => array(
				'style1' => esc_html__('Layout One', 'insurance-lite'),
				'style2' => esc_html__('Layout Two', 'insurance-lite'),
				'style3' => esc_html__('Layout Three', 'insurance-lite'),
				'layout_four' => esc_html__('Layout Four', 'insurance-lite'),
			),
			'priority' => 9
		));


		/**
		 * Client Logo Section
		 */
		$wp_customize->add_setting('construction_light_client_bg', array(
            'default' => '',
            'sanitize_callback' => 'sanitize_hex_color',
            // 'transport' => 'postMessage'
        ));
        
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'construction_light_client_bg', array(
            'section' => 'construction_light_client_section',
            'label' => esc_html__('Background', 'insurance-lite')
		)));

		$wp_customize->add_setting( 'construction_light_client_inverse', array(
			'default' => true,
			// 'transport' => 'postMessage',
			'sanitize_callback' => 'construction_light_sanitize_checkbox'			//done
		));

		$wp_customize->add_control( 'construction_light_client_inverse', array(
			'label'    => esc_html__( 'Logo Background', 'insurance-lite' ),
			'section'  => 'construction_light_client_section',
			'type'     => 'checkbox'
		));


    }
}
add_action( 'customize_register' , 'insurance_lite_child_options', 11 );
if ( class_exists( 'WP_Customize_Control' ) && ! class_exists( 'InsuranceLiteInfoText' ) ) :
    /**
     * Info Text Control
     */
    class InsuranceLiteInfoText extends WP_Customize_Control {
        public function render_content() {
            ?>
            <span class="customize-control-title">
                <?php echo esc_html($this->label); ?>
            </span>
            <?php if ($this->description) { ?>
                <span class="customize-control-description">
                    <?php echo wp_kses_post($this->description); ?>
                </span>
                <?php
            }
        }
    }
endif;