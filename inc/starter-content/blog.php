<?php
/**
 * Contact starter content.
 */
return array(
	'post_type'    => 'page',
	'post_title'   => _x( 'Blog', 'Theme starter content', 'insurance-lite' ),
	'template' => 'template-pagebuilder.php',
	'post_content' => '
	<!-- wp:pattern {"slug":"insurance-lite/breadcrumb"} /-->
	<!-- wp:spacer {"height":61} -->
	<div style="height:61px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->
	
	<!-- wp:pattern {"slug":"insurance-lite/latest-news"} /-->

	<!-- wp:spacer {"height":61} -->
	<div style="height:61px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->',
);