<?php
/**
 * Home starter content.
 */
$default_home_content = '
	<!-- wp:pattern {"slug":"insurance-lite/about"} /-->
	<!-- wp:pattern {"slug":"insurance-lite/features"} /-->
	<!-- wp:pattern {"slug":"insurance-lite/call-to-action"} /-->
	<!-- wp:pattern {"slug":"insurance-lite/service"} /-->
	<!-- wp:pattern {"slug":"insurance-lite/team"} /-->
	<!-- wp:pattern {"slug":"insurance-lite/service-list"} /-->
	<!-- wp:pattern {"slug":"insurance-lite/contact"} /-->
';

return array(
	'post_type'    => 'page',
	'post_title'   => _x( 'Home', 'Theme starter content', 'insurance-lite' ),
	'post_content' => $default_home_content,
);