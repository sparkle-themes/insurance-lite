<?php
/**
 * Contact starter content.
 */
return array(
	'post_type'    => 'page',
	'post_title'   => _x( 'About', 'Theme starter content', 'insurance-lite' ),
	'thumbnail'    => '{{featured-image-home}}',
	'template' => 'template-pagebuilder.php',
	'post_content' => '
		<!-- wp:pattern {"slug":"insurance-lite/breadcrumb"} /-->
		<!-- wp:pattern {"slug":"insurance-lite/about"} /-->
		<!-- wp:pattern {"slug":"insurance-lite/service-list"} /-->
		<!-- wp:pattern {"slug":"insurance-lite/service"} /-->
	',
);