<?php
/**
 * Service starter content.
 */
return array(
	'post_type'    => 'page',
	'post_title'   => _x( 'Service', 'Theme starter content', 'insurance-lite' ),
	'thumbnail'    => '{{featured-image-home}}',
	'construction_light_page_layouts' => 'no',
	'template' => 'template-pagebuilder.php',
	'post_content' => '
    <!-- wp:pattern {"slug":"insurance-lite/breadcrumb"} /-->
    <!-- wp:spacer {"height":61} -->
	<div style="height:61px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->
    <!-- wp:pattern {"slug":"insurance-lite/features"} /-->
    <!-- wp:pattern {"slug":"insurance-lite/service"} /-->
    <!-- wp:pattern {"slug":"insurance-lite/call-to-action"} /-->
    <!-- wp:pattern {"slug":"insurance-lite/service-list"} /-->

    <!-- wp:pattern {"slug":"insurance-lite/call-to-action"} /-->

    <!-- wp:spacer {"height":61} -->
	<div style="height:61px" aria-hidden="true" class="wp-block-spacer"></div>
	<!-- /wp:spacer -->

    
    ',
);