<?php
/**
 * Describe child theme functions
 *
 * @package Construction Light
 * @subpackage Insurance Lite
 * 
 */

 if ( ! function_exists( 'insurance_lite_setup' ) ) :

    function insurance_lite_setup() {
		/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on Insurance Lite, use a find and replace
		* to change 'insurance-lite' to the name of your theme in all the template files.
		*/
		load_theme_textdomain( 'insurance-lite', get_template_directory() . '/languages' );

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
        
        $insurance_lite_theme_info = wp_get_theme();
        $GLOBALS['insurance_lite_version'] = $insurance_lite_theme_info->get( 'Version' );

		add_theme_support( "title-tag" );
		add_theme_support( 'automatic-feed-links' );
    }
endif;
add_action( 'after_setup_theme', 'insurance_lite_setup' );


/**
 * Enqueue child theme styles and scripts
*/
function insurance_lite_scripts() {
    
    global $insurance_lite_version;

	wp_dequeue_style( 'construction-light-fonts');
	wp_dequeue_style( 'construction-light-style' );
    
    wp_enqueue_style( 'insurance-lite-parent-style', trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'style.css', array(), esc_attr( $insurance_lite_version ) );
    
	wp_enqueue_style( 'insurance-lite-fonts', insurance_lite_fonts_url(), array(), null );

    wp_enqueue_style( 'insurance-lite-responsive', get_template_directory_uri(). '/assets/css/responsive.css');
    
    wp_enqueue_style( 'insurance-lite-style', get_stylesheet_uri(), esc_attr( $insurance_lite_version ) );

    wp_enqueue_script('insurance-lite', get_stylesheet_directory_uri() . '/js/insurance-lite.js', array('jquery','masonry'), esc_attr( $insurance_lite_version ), true);

	if ( is_rtl() ) {
		wp_enqueue_style( 'construction-light-rtl', trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'rtl.css', array(), esc_attr( $insurance_lite_version ) );	
	}

}
add_action( 'wp_enqueue_scripts', 'insurance_lite_scripts', 20 );

function insurance_lite_css_strip_whitespace($css) {
    $replace = array(
        "#/\*.*?\*/#s" => "", // Strip C style comments.
        "#\s\s+#" => " ", // Strip excess whitespace.
    );
    $search = array_keys($replace);
    $css = preg_replace($search, $replace, $css);

    $replace = array(
        ": " => ":",
        "; " => ";",
        " {" => "{",
        " }" => "}",
        ", " => ",",
        "{ " => "{",
        ";}" => "}", // Strip optional semicolons.
        ",\n" => ",", // Don't wrap multiple selectors.
        "\n}" => "}", // Don't wrap closing braces.
        "} " => "}\n", // Put each rule on it's own line.
    );
    $search = array_keys($replace);
    $css = str_replace($search, $replace, $css);

    return trim($css);
}

/**
 * Dynamic Style
 */
add_filter( 'construction-light-dynamic-css', 'insurance_lite_dymanic_styles', 100 );
function insurance_lite_dymanic_styles($dynamic_css) {
    
    $services_bg = get_theme_mod('construction_light_service_image');
 
    $primar_color = esc_html( get_theme_mod('construction_light_primary_color', '#fc5e16') );
	if($primar_color){
		
		$dynamic_css .= "
		.cl-contact-section.cons_light_feature .feature-list .icon-box,
		.cons_light_feature .feature-list .icon-box,
		.about_us_front .achivement-items.list .achivement-icon i,
		.site-header:not(.headertwo) .nav-classic .site-branding h1 a,
		.cons_light_feature.layout_four .feature-list .icon-box{
			color: {$primar_color};
		}
		.cons_light_team_layout_two.layout_three .team-wrap,
		.about_us_front .achivement-items ul li{
			border-color: {$primar_color};
		}
		.cl-client-logo-inverse .owl-carousel.owl-theme.client_logo,
		.hl-border,
		.testimonial-slider .item:before,
		header.cons-agency .nav-classic{
			background-color: {$primar_color};
		}
		.cl-client-logo-inverse .owl-carousel.owl-theme.client_logo:after,
		.cl-client-logo-inverse .owl-carousel.owl-theme.client_logo:before{
			background-color: {$primar_color}33;
		}
		#cl-promoservice-section.cons_light_feature.style1 .feature-list .box figure::before{
			background: linear-gradient(45deg, #e2e3e9, {$primar_color});
		}
		";
	}
	$header_text_color = get_header_textcolor();
	if( $header_text_color == '000000'){
		$header_text_color = '0162ca';
	}
	$header_text_color = esc_html( $header_text_color );
	if( $header_text_color ){
		$dynamic_css .="
		.site-header:not(.headertwo) .nav-classic .site-branding h1 a{
			color: #{$header_text_color};
		}";
	}


	$construction_light_aboutus_text_color = esc_html ( get_theme_mod('construction_light_aboutus_text_color') );
	if($construction_light_aboutus_text_color) {
		$dynamic_css .="
			svg.radial-progress text{ fill: {$construction_light_aboutus_text_color};
		";
	}

	
	if ( has_header_image() ) {
		$dynamic_css .= '.nav-classic{ background-image: url("' . esc_url( get_header_image() ) . '"); background-repeat: no-repeat; background-position: center center; background-size: cover; }';
	}
	
	wp_add_inline_style( 'insurance-lite-style', insurance_lite_css_strip_whitespace($dynamic_css) );
}


if( !function_exists('insurance_lite_fonts_url')):
	function insurance_lite_fonts_url() {
        $fonts_url = '';
        $font_families = array();
        
        if ( 'off' !== _x( 'on', 'Oswald: on or off', 'insurance-lite' ) ) {
            $font_families[] = 'Oswald:200,300,400,600,700,800';
        }
        if ( 'off' !== _x( 'on', 'Open Sans: on or off', 'insurance-lite' ) ) {
            $font_families[] = 'Open Sans:300,400,600,700,800';
        }
        if( $font_families ) {

            $query_args = array(

                'family' => urlencode( implode( '|', $font_families ) ),
                'subset' => urlencode( 'latin,latin-ext' ),
            );

            $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        }
        return esc_url ( $fonts_url );
    }
endif;

if( !function_exists('insurance_lite_allow_iframes')):
	function insurance_lite_allow_iframes( $allowedposttags ){

		$allowedposttags['iframe'] = array(
			'align' => true,
			'allow' => true,
			'allowfullscreen' => true,
			'class' => true,
			'frameborder' => true,
			'height' => true,
			'id' => true,
			'marginheight' => true,
			'marginwidth' => true,
			'name' => true,
			'scrolling' => true,
			'src' => true,
			'style' => true,
			'width' => true,
			'allowFullScreen' => true,
			'class' => true,
			'frameborder' => true,
			'height' => true,
			'mozallowfullscreen' => true,
			'src' => true,
			'title' => true,
			'webkitAllowFullScreen' => true,
			'width' => true
		);

		return $allowedposttags;
	}
	add_filter( 'wp_kses_allowed_html', 'insurance_lite_allow_iframes', 1 );
endif;



// The filter callback function.
function insurance_lite_primary_color( $color ) {
    return "#0162CA";
}
add_filter( 'construction_light_primary_color', 'insurance_lite_primary_color', 10, 1 );

function insurance_lite_header_textcolor(){
	return '#0162CA';
}
add_filter( 'header_textcolor', 'insurance_lite_header_textcolor', 10, 1 );

/** hero slider content class */
function insurance_lite_slider_class(){
	return 'col-lg-9 cl-center-content';
}
add_filter( 'construction-light-slider-class', 'insurance_lite_slider_class', 10, 1 );

/** client logo section */
function insurance_lite_client_logo_class(){
	if( get_theme_mod('construction_light_client_inverse', true) == true){
		return "cl-client-logo-inverse";
	}
}
add_filter( 'construction-light-client-logo-class', 'insurance_lite_client_logo_class', 10, 1 );

/** welcome page pro link */
function insurance_lite_pro_link(){
	return 'https://sparklewpthemes.com/wordpress-themes/insurance-multipurpose-wordpress-theme/?wpam_id=44';
}
add_filter( 'construction-light-pro-link', 'insurance_lite_pro_link', 10, 1 );
/** welcome page demo link */
function insurance_lite_demo_link(){
	return 'https://demo.sparklewpthemes.com/constructionlight/insurance-lite/';
}
add_filter( 'construction-light-demo-link', 'insurance_lite_demo_link', 10, 1 );

/** welcome page pro demo link */
function insurance_lite_pro_demo_link(){
	return 'https://demo.sparklewpthemes.com/constructionlight/insurance-lite/';
}
add_filter( 'construction-light-pro-demo-link', 'insurance_lite_pro_demo_link', 10, 1 );

/** include files */
require get_stylesheet_directory() . '/inc/theme-functions.php';
require get_stylesheet_directory() . '/inc/customizer.php';
/**
 * starter content
 */
require get_stylesheet_directory() .'/inc/starter-content/init.php';


add_filter('construction_light_fse_block_pattern_categories', function(){
	return array(
		'insurance-lite' => array( 'label' => __( 'Insurance Lite', 'insurance-lite' ) )
	);
});