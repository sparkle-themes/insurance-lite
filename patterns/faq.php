<?php
 /**
  * Title: FAQ
  * Slug: insurance-lite/faq
  * Categories: insurance-lite
  */
?>
<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide" style="padding-top:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70)"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"width":"40%"} -->
<div class="wp-block-column" style="flex-basis:40%"><!-- wp:heading -->
<h2 class="wp-block-heading">Most Commonly Asked Questions About Us.</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"fontSize":"medium"} -->
<p class="has-medium-font-size">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"medium"} -->
<p class="has-medium-font-size"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"medium"} -->
<p class="has-medium-font-size">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"medium"} -->
<p class="has-medium-font-size"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"style":{"border":{"radius":"5px"},"spacing":{"padding":{"top":"var:preset|spacing|40","right":"var:preset|spacing|70","bottom":"var:preset|spacing|40","left":"var:preset|spacing|70"}}},"className":"is-style-fill"} -->
<div class="wp-block-button is-style-fill"><a class="wp-block-button__link wp-element-button" style="border-radius:5px;padding-top:var(--wp--preset--spacing--40);padding-right:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--40);padding-left:var(--wp--preset--spacing--70)">Quick Contact</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"align":"full","className":"has-accordion active","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group alignfull has-accordion active"><!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">What is different between Single Site and Unlimited Site license?</h4>
<!-- /wp:heading -->

<!-- wp:group {"className":"accordion-content","layout":{"type":"constrained"}} -->
<div class="wp-block-group accordion-content"><!-- wp:paragraph -->
<p>After purchasing the theme, you will need to add the license code. With Single Site License, you can use the extension in one website/domain only. But with Unlimited Site License, you can use it in as many website/domain as you want. There is no limit.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As it is a one time payment with lifetime use, you can use it in as many website as you want in future as well.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","className":"has-accordion","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group alignfull has-accordion"><!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">What is different between Single Site and Unlimited Site license?</h4>
<!-- /wp:heading -->

<!-- wp:group {"className":"accordion-content","layout":{"type":"constrained"}} -->
<div class="wp-block-group accordion-content"><!-- wp:paragraph -->
<p>After purchasing the theme, you will need to add the license code. With Single Site License, you can use the extension in one website/domain only. But with Unlimited Site License, you can use it in as many website/domain as you want. There is no limit.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As it is a one time payment with lifetime use, you can use it in as many website as you want in future as well.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"border":{"width":"1px"},"spacing":{"padding":{"top":"var:preset|spacing|30","right":"var:preset|spacing|30","bottom":"var:preset|spacing|30","left":"var:preset|spacing|30"}}},"className":"has-accordion","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group alignfull has-accordion" style="border-width:1px;padding-top:var(--wp--preset--spacing--30);padding-right:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30);padding-left:var(--wp--preset--spacing--30)"><!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">What is different between Single Site and Unlimited Site license?</h4>
<!-- /wp:heading -->

<!-- wp:group {"className":"accordion-content","layout":{"type":"constrained"}} -->
<div class="wp-block-group accordion-content"><!-- wp:paragraph -->
<p>After purchasing the theme, you will need to add the license code. With Single Site License, you can use the extension in one website/domain only. But with Unlimited Site License, you can use it in as many website/domain as you want. There is no limit.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As it is a one time payment with lifetime use, you can use it in as many website as you want in future as well.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->