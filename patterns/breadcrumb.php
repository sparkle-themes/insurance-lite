<?php
 /**
  * Title: Breadcrumb
  * Slug: insurance-lite/breadcrumb
  * Categories: insurance-lite
  */
?>
<!-- wp:cover {"url":"<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/banner3-1.jpeg","id":174,"dimRatio":50,"isDark":false,"align":"full","className":"breadcrumb"} -->
<div class="wp-block-cover alignfull is-light breadcrumb"><span aria-hidden="true" class="wp-block-cover__background has-background-dim"></span><img class="wp-block-cover__image-background wp-image-174" alt="" src="<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/banner3-1.jpeg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:post-title {"textAlign":"center","textColor":"background"} /--></div></div>
<!-- /wp:cover -->