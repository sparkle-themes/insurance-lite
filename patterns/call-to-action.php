<?php
 /**
  * Title: Call to Action
  * Slug: insurance-lite/call-to-action
  * Categories: insurance-lite
  */
?>
<!-- wp:group {"align":"full","style":{"spacing":{"blockGap":"0px","padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}},"backgroundColor":"foreground","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignfull has-foreground-background-color has-background" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:cover {"url":"<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/banner3-1.jpeg","id":172,"hasParallax":true,"dimRatio":40,"customGradient":"linear-gradient(93deg,rgb(0,0,0) 38%,rgba(46,42,48,0.08) 94%)","contentPosition":"center center","isDark":false,"align":"full","style":{"spacing":{"padding":{"top":"50px","bottom":"50px","right":"50px","left":"50px"}}}} -->
<div class="wp-block-cover alignfull is-light has-parallax" style="padding-top:50px;padding-right:50px;padding-bottom:50px;padding-left:50px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-40 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(93deg,rgb(0,0,0) 38%,rgba(46,42,48,0.08) 94%)"></span><div role="img" class="wp-block-cover__image-background wp-image-172 has-parallax" style="background-position:50% 50%;background-image:url(<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/banner3-1.jpeg)"></div><div class="wp-block-cover__inner-container"><!-- wp:group {"align":"wide","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide"><!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"100px","bottom":"100px"},"blockGap":"10px"},"border":{"style":"solid","width":"2px","radius":"5px"}},"textColor":"background","layout":{"inherit":false}} -->
<div class="wp-block-group alignwide has-background-color has-text-color" style="border-style:solid;border-width:2px;border-radius:5px;padding-top:100px;padding-bottom:100px"><!-- wp:paragraph {"align":"center","textColor":"white","className":" animated animated-fadeInUp","fontSize":"content-heading"} -->
<p class="has-text-align-center animated animated-fadeInUp has-white-color has-text-color has-content-heading-font-size">We Help Our Clients</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"textAlign":"center","align":"wide","style":{"typography":{"fontStyle":"normal","fontWeight":"700"}},"className":" animated animated-fadeInUp","fontSize":"huge"} -->
<h2 class="wp-block-heading alignwide has-text-align-center animated animated-fadeInUp has-huge-font-size" style="font-style:normal;font-weight:700">We Help You to Grow Your</h2>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"center","align":"wide","style":{"typography":{"fontStyle":"normal","fontWeight":"700"}},"className":" animated animated-fadeInUp","fontSize":"huge"} -->
<h2 class="wp-block-heading alignwide has-text-align-center animated animated-fadeInUp has-huge-font-size" style="font-style:normal;font-weight:700">Business Quickly</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0px"}}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","className":" animated animated-fadeInUp","fontSize":"content-heading"} -->
<p class="has-text-align-center animated animated-fadeInUp has-content-heading-font-size">Change the workflow for your WordPress site, control everything from Site editor without</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","className":" animated animated-fadeInUp","fontSize":"content-heading"} -->
<p class="has-text-align-center animated animated-fadeInUp has-content-heading-font-size">using any extra plugins.  Improved speed, web vitals score and SEO to get better positions</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:buttons {"className":"animated fadeInUp","layout":{"type":"flex","justifyContent":"center"},"style":{"spacing":{"blockGap":"10px","margin":{"top":"40px","bottom":"20px"}}}} -->
<div class="wp-block-buttons animated fadeInUp" style="margin-top:40px;margin-bottom:20px"><!-- wp:button {"backgroundColor":"primary","style":{"spacing":{"padding":{"right":"35px","left":"35px"}},"border":{"radius":"5px"}},"className":"animated animated-fadeInUp is-style-fill"} -->
<div class="wp-block-button animated animated-fadeInUp is-style-fill"><a class="wp-block-button__link has-primary-background-color has-background wp-element-button" href="#" style="border-radius:5px;padding-right:35px;padding-left:35px">Get Started</a></div>
<!-- /wp:button -->

<!-- wp:button {"backgroundColor":"white","textColor":"black","style":{"spacing":{"padding":{"right":"35px","left":"35px"}},"border":{"radius":"5px"}},"className":"animated animated-fadeInUp is-style-fill"} -->
<div class="wp-block-button animated animated-fadeInUp is-style-fill"><a class="wp-block-button__link has-black-color has-white-background-color has-text-color has-background wp-element-button" href="#" style="border-radius:5px;padding-right:35px;padding-left:35px">Contact Us</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->