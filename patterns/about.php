<?php
 /**
  * Title: About Us
  * Slug: insurance-lite/about
  * Categories: insurance-lite
  */
?>
<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"100px","bottom":"100px","right":"0px","left":"0px"},"blockGap":"0"}},"backgroundColor":"background","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignfull has-background-background-color has-background" style="padding-top:100px;padding-right:0px;padding-bottom:100px;padding-left:0px"><!-- wp:columns {"align":"wide","style":{"spacing":{"blockGap":{"top":"0","left":"0"},"padding":{"top":"0","right":"0","bottom":"0","left":"0"},"margin":{"top":"0","bottom":"0"}}}} -->
<div class="wp-block-columns alignwide" style="margin-top:0;margin-bottom:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"><!-- wp:column {"verticalAlignment":"center","width":"35%","style":{"spacing":{"padding":{"top":"0","right":"0","bottom":"0","left":"0"}}}} -->
<div class="wp-block-column is-vertically-aligned-center" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;flex-basis:35%"><!-- wp:group {"style":{"border":{"radius":"10px"}},"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp" style="border-radius:10px"><!-- wp:cover {"url":"https://demo.sparklewpthemes.com/constructionlight/insurance-lite/wp-content/uploads/sites/48/2022/07/Screen-Shot-2022-07-24-at-15.12-1.png","dimRatio":0,"isDark":false,"style":{"color":{}}} -->
<div class="wp-block-cover is-light"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background" alt="" src="https://demo.sparklewpthemes.com/constructionlight/insurance-lite/wp-content/uploads/sites/48/2022/07/Screen-Shot-2022-07-24-at-15.12-1.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:paragraph {"align":"center","fontSize":"large"} -->
<p class="has-text-align-center has-large-font-size"></p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"top","width":"","style":{"spacing":{"padding":{"top":"0px","right":"0px","left":"40px","bottom":"0px"}}},"fontSize":"content-heading"} -->
<div class="wp-block-column is-vertically-aligned-top has-content-heading-font-size" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:40px"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"20px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px"},"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:0px"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"700","lineHeight":"1.3"}},"fontSize":"slider-title","fontFamily":"oswold"} -->
<h2 class="wp-block-heading has-oswold-font-family has-slider-title-font-size" style="font-style:normal;font-weight:700;line-height:1.3">We Provide Best Insurance Policy for Any Purpose</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"200"}},"textColor":"foreground","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-foreground-color has-text-color has-poppins-font-family has-medium-font-size" style="font-style:normal;font-weight:200">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text.</p>
<!-- /wp:paragraph -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"className":"has-shadow-dark"} -->
<div class="wp-block-column has-shadow-dark"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","left":"15px","right":"15px"},"blockGap":"15px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group" style="padding-top:20px;padding-right:15px;padding-bottom:20px;padding-left:15px"><!-- wp:group {"style":{"spacing":{"padding":{"left":"0px","right":"0","top":"0"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:0;padding-right:0;padding-left:0px"><!-- wp:paragraph {"fontSize":"huge"} -->
<p class="has-huge-font-size">📋</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"800","lineHeight":"1.2","fontSize":"24px"}}} -->
<p class="has-text-align-left" style="font-size:24px;font-style:normal;font-weight:800;line-height:1.2">Business Service</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","fontSize":"medium","fontFamily":"oswold"} -->
<p class="has-text-align-left has-foreground-color has-text-color has-oswold-font-family has-medium-font-size" style="font-style:normal;font-weight:300">Quickly productive just time strategic theme.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"className":"has-shadow-dark"} -->
<div class="wp-block-column has-shadow-dark"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","left":"26px","right":"26px"},"blockGap":"15px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group" style="padding-top:20px;padding-right:26px;padding-bottom:20px;padding-left:26px"><!-- wp:group {"style":{"spacing":{"padding":{"left":"0px","right":"0","top":"0"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:0;padding-right:0;padding-left:0px"><!-- wp:paragraph {"fontSize":"huge"} -->
<p class="has-huge-font-size">💵</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"800","lineHeight":"1.2","fontSize":"24px"}},"fontFamily":"oswold"} -->
<p class="has-text-align-left has-oswold-font-family" style="font-size:24px;font-style:normal;font-weight:800;line-height:1.2">Marketing Plan</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","fontSize":"medium","fontFamily":"oswold"} -->
<p class="has-text-align-left has-foreground-color has-text-color has-oswold-font-family has-medium-font-size" style="font-style:normal;font-weight:300">Quickly productive just in time strategic theme.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"className":"has-shadow-dark"} -->
<div class="wp-block-column has-shadow-dark"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","left":"15px","right":"15px"},"blockGap":"13px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group" style="padding-top:20px;padding-right:15px;padding-bottom:20px;padding-left:15px"><!-- wp:group {"style":{"spacing":{"padding":{"left":"0px","right":"0","top":"0"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:0;padding-right:0;padding-left:0px"><!-- wp:paragraph {"fontSize":"huge"} -->
<p class="has-huge-font-size">👤</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"800","lineHeight":"1.2","fontSize":"24px"}}} -->
<p class="has-text-align-left" style="font-size:24px;font-style:normal;font-weight:800;line-height:1.2">Team Management</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","fontSize":"medium","fontFamily":"oswold"} -->
<p class="has-text-align-left has-foreground-color has-text-color has-oswold-font-family has-medium-font-size" style="font-style:normal;font-weight:300">Quickly productive just time strategic theme.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"className":"has-shadow-dark"} -->
<div class="wp-block-column has-shadow-dark"><!-- wp:group {"style":{"spacing":{"padding":{"top":"20px","bottom":"20px","left":"26px","right":"26px"},"blockGap":"15px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group" style="padding-top:20px;padding-right:26px;padding-bottom:20px;padding-left:26px"><!-- wp:group {"style":{"spacing":{"padding":{"left":"0px","right":"0","top":"0"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:0;padding-right:0;padding-left:0px"><!-- wp:paragraph {"fontSize":"huge"} -->
<p class="has-huge-font-size">🏆</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"800","lineHeight":"1.2","fontSize":"24px"}},"fontFamily":"oswold"} -->
<p class="has-text-align-left has-oswold-font-family" style="font-size:24px;font-style:normal;font-weight:800;line-height:1.2">Award Won</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"300"}},"textColor":"foreground","fontSize":"medium","fontFamily":"oswold"} -->
<p class="has-text-align-left has-foreground-color has-text-color has-oswold-font-family has-medium-font-size" style="font-style:normal;font-weight:300">Quickly productive just in time strategic theme.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->