<?php
 /**
  * Title: Service
  * Slug: insurance-lite/service
  * Categories: insurance-lite
  */
?>


<!-- wp:group {"align":"full","style":{"border":{"width":"0px","style":"none"},"spacing":{"blockGap":"0","padding":{"top":"80px","right":"0","bottom":"80px","left":"0"}}},"backgroundColor":"background","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignfull has-background-background-color has-background" style="border-style:none;border-width:0px;padding-top:80px;padding-right:0;padding-bottom:80px;padding-left:0"><!-- wp:group {"align":"full","style":{"spacing":{"blockGap":"0","padding":{"top":"0","right":"0","bottom":"0","left":"0"},"margin":{"top":"0","bottom":"0"}}},"className":"equal-height-column equal-height-img","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull equal-height-column equal-height-img" style="margin-top:0;margin-bottom:0;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"><!-- wp:columns {"align":"full","style":{"spacing":{"blockGap":{"top":"0","left":"0"},"padding":{"right":"0","left":"0","top":"0","bottom":"0"}}}} -->
<div class="wp-block-columns alignfull" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"><!-- wp:column {"verticalAlignment":"center","width":"25%","className":"wrap"} -->
<div class="wp-block-column is-vertically-aligned-center wrap" style="flex-basis:25%"><!-- wp:cover {"url":"<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/doctor-filling-up-life-insurance-form.jpeg","id":14,"dimRatio":0,"minHeight":400,"minHeightUnit":"px","isDark":false,"className":"image-zoom-hover","style":{"color":{}}} -->
<div class="wp-block-cover is-light image-zoom-hover" style="min-height:400px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-14" alt="" src="<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/doctor-filling-up-life-insurance-form.jpeg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:paragraph {"align":"center","fontSize":"large"} -->
<p class="has-text-align-center has-large-font-size"></p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center","width":"25%"} -->
<div class="wp-block-column is-vertically-aligned-center" style="flex-basis:25%"><!-- wp:cover {"overlayColor":"primary","minHeight":400,"style":{"spacing":{"padding":{"top":"var:preset|spacing|30","right":"var:preset|spacing|30","bottom":"var:preset|spacing|30","left":"var:preset|spacing|30"}}}} -->
<div class="wp-block-cover" style="padding-top:var(--wp--preset--spacing--30);padding-right:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30);padding-left:var(--wp--preset--spacing--30);min-height:400px"><span aria-hidden="true" class="wp-block-cover__background has-primary-background-color has-background-dim-100 has-background-dim"></span><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"padding":{"right":"15px","left":"15px","top":"66px","bottom":"66px"},"blockGap":"20px"}},"textColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background-color has-text-color" style="padding-top:66px;padding-right:15px;padding-bottom:66px;padding-left:15px"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"400"}},"textColor":"background","fontSize":"slider-title"} -->
<h2 class="wp-block-heading has-text-align-center has-background-color has-text-color has-slider-title-font-size" style="font-style:normal;font-weight:400">Health Insurance</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-color has-text-color has-poppins-font-family" style="font-size:16px;font-style:normal;font-weight:300">We have almost 35+ years of experience for providing consulting services solutions</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"align":"wide","layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons alignwide"><!-- wp:button {"textColor":"background","style":{"border":{"radius":"25px"},"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"300"},"spacing":{"padding":{"right":"18px","left":"18px","top":"6px","bottom":"6px"}}},"className":"is-style-outline"} -->
<div class="wp-block-button has-custom-font-size is-style-outline" style="font-size:16px;font-style:normal;font-weight:300"><a class="wp-block-button__link has-background-color has-text-color wp-element-button" href="#" style="border-radius:25px;padding-top:6px;padding-right:18px;padding-bottom:6px;padding-left:18px">Explore More</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center","width":"25%","className":"wrap"} -->
<div class="wp-block-column is-vertically-aligned-center wrap" style="flex-basis:25%"><!-- wp:cover {"url":"<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/banner3-1.jpeg","id":174,"dimRatio":0,"minHeight":400,"minHeightUnit":"px","isDark":false,"className":"image-zoom-hover","style":{"color":{}}} -->
<div class="wp-block-cover is-light image-zoom-hover" style="min-height:400px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-174" alt="" src="<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/banner3-1.jpeg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:paragraph {"align":"center","fontSize":"large"} -->
<p class="has-text-align-center has-large-font-size"></p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center","width":"25%"} -->
<div class="wp-block-column is-vertically-aligned-center" style="flex-basis:25%"><!-- wp:cover {"customOverlayColor":"#29282d","minHeight":400,"style":{"spacing":{"padding":{"top":"var:preset|spacing|30","right":"var:preset|spacing|30","bottom":"var:preset|spacing|30","left":"var:preset|spacing|30"}}}} -->
<div class="wp-block-cover" style="padding-top:var(--wp--preset--spacing--30);padding-right:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30);padding-left:var(--wp--preset--spacing--30);min-height:400px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-100 has-background-dim" style="background-color:#29282d"></span><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"padding":{"right":"15px","left":"15px","top":"66px","bottom":"66px"},"blockGap":"20px"}},"textColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background-color has-text-color" style="padding-top:66px;padding-right:15px;padding-bottom:66px;padding-left:15px"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"400"}},"textColor":"background","fontSize":"slider-title"} -->
<h2 class="wp-block-heading has-text-align-center has-background-color has-text-color has-slider-title-font-size" style="font-style:normal;font-weight:400">Life Insurance</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-color has-text-color has-poppins-font-family" style="font-size:16px;font-style:normal;font-weight:300">We have almost 35+ years of experience for providing consulting services solutions</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"align":"wide","layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons alignwide"><!-- wp:button {"textColor":"background","style":{"border":{"radius":"25px"},"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"300"},"spacing":{"padding":{"top":"6px","bottom":"6px","left":"18px","right":"18px"}}},"className":"is-style-outline"} -->
<div class="wp-block-button has-custom-font-size is-style-outline" style="font-size:16px;font-style:normal;font-weight:300"><a class="wp-block-button__link has-background-color has-text-color wp-element-button" href="#" style="border-radius:25px;padding-top:6px;padding-right:18px;padding-bottom:6px;padding-left:18px">Explore More</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"align":"full","style":{"spacing":{"blockGap":{"top":"0","left":"0"},"padding":{"right":"0","left":"0","top":"0","bottom":"0"}}}} -->
<div class="wp-block-columns alignfull" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"><!-- wp:column {"verticalAlignment":"center","width":"25%"} -->
<div class="wp-block-column is-vertically-aligned-center" style="flex-basis:25%"><!-- wp:cover {"customOverlayColor":"#29282d","minHeight":400,"style":{"spacing":{"padding":{"top":"var:preset|spacing|30","right":"var:preset|spacing|30","bottom":"var:preset|spacing|30","left":"var:preset|spacing|30"}}}} -->
<div class="wp-block-cover" style="padding-top:var(--wp--preset--spacing--30);padding-right:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30);padding-left:var(--wp--preset--spacing--30);min-height:400px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-100 has-background-dim" style="background-color:#29282d"></span><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"padding":{"right":"15px","left":"15px","top":"66px","bottom":"66px"},"blockGap":"20px"}},"textColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background-color has-text-color" style="padding-top:66px;padding-right:15px;padding-bottom:66px;padding-left:15px"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"400"}},"textColor":"background","fontSize":"slider-title"} -->
<h2 class="wp-block-heading has-text-align-center has-background-color has-text-color has-slider-title-font-size" style="font-style:normal;font-weight:400">Business Insurance</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-color has-text-color has-poppins-font-family" style="font-size:16px;font-style:normal;font-weight:300">We have almost 35+ years of experience for providing consulting services solutions</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"align":"wide","layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons alignwide"><!-- wp:button {"textColor":"background","style":{"border":{"radius":"25px"},"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"300"},"spacing":{"padding":{"top":"6px","bottom":"6px","left":"18px","right":"18px"}}},"className":"is-style-outline"} -->
<div class="wp-block-button has-custom-font-size is-style-outline" style="font-size:16px;font-style:normal;font-weight:300"><a class="wp-block-button__link has-background-color has-text-color wp-element-button" href="#" style="border-radius:25px;padding-top:6px;padding-right:18px;padding-bottom:6px;padding-left:18px">Explore More</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center","width":"25%","className":"wrap"} -->
<div class="wp-block-column is-vertically-aligned-center wrap" style="flex-basis:25%"><!-- wp:cover {"url":"<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/family-moving-using-boxes-1-1024x683.jpeg","id":171,"dimRatio":0,"minHeight":400,"minHeightUnit":"px","isDark":false,"className":"image-zoom-hover","style":{"color":{}}} -->
<div class="wp-block-cover is-light image-zoom-hover" style="min-height:400px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-171" alt="" src="<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/family-moving-using-boxes-1-1024x683.jpeg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:paragraph {"align":"center","fontSize":"large"} -->
<p class="has-text-align-center has-large-font-size"></p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center","width":"25%"} -->
<div class="wp-block-column is-vertically-aligned-center" style="flex-basis:25%"><!-- wp:cover {"overlayColor":"primary","minHeight":400,"style":{"spacing":{"padding":{"top":"var:preset|spacing|30","right":"var:preset|spacing|30","bottom":"var:preset|spacing|30","left":"var:preset|spacing|30"}}}} -->
<div class="wp-block-cover" style="padding-top:var(--wp--preset--spacing--30);padding-right:var(--wp--preset--spacing--30);padding-bottom:var(--wp--preset--spacing--30);padding-left:var(--wp--preset--spacing--30);min-height:400px"><span aria-hidden="true" class="wp-block-cover__background has-primary-background-color has-background-dim-100 has-background-dim"></span><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"padding":{"right":"15px","left":"15px","top":"66px","bottom":"66px"},"blockGap":"20px"}},"textColor":"background","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-background-color has-text-color" style="padding-top:66px;padding-right:15px;padding-bottom:66px;padding-left:15px"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"400"}},"textColor":"background","fontSize":"slider-title"} -->
<h2 class="wp-block-heading has-text-align-center has-background-color has-text-color has-slider-title-font-size" style="font-style:normal;font-weight:400">Car Insurance</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"300"}},"textColor":"background","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-color has-text-color has-poppins-font-family" style="font-size:16px;font-style:normal;font-weight:300">We have almost 35+ years of experience for providing consulting services solutions</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"align":"wide","layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons alignwide"><!-- wp:button {"textColor":"background","style":{"border":{"radius":"25px"},"typography":{"fontSize":"16px","fontStyle":"normal","fontWeight":"300"},"spacing":{"padding":{"top":"6px","bottom":"6px","left":"18px","right":"18px"}}},"className":"is-style-outline"} -->
<div class="wp-block-button has-custom-font-size is-style-outline" style="font-size:16px;font-style:normal;font-weight:300"><a class="wp-block-button__link has-background-color has-text-color wp-element-button" href="#" style="border-radius:25px;padding-top:6px;padding-right:18px;padding-bottom:6px;padding-left:18px">Explore More</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center","width":"25%","className":"wrap"} -->
<div class="wp-block-column is-vertically-aligned-center wrap" style="flex-basis:25%"><!-- wp:cover {"url":"<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/happy-customer-buying-brand-new-car-local-car-dealership-1024x684.jpeg","id":47,"dimRatio":0,"minHeight":400,"minHeightUnit":"px","contentPosition":"center center","isDark":false,"className":"image-zoom-hover","style":{"color":{}}} -->
<div class="wp-block-cover is-light image-zoom-hover" style="min-height:400px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-47" alt="" src="<?php echo esc_url( get_stylesheet_directory_uri() );?>/images/happy-customer-buying-brand-new-car-local-car-dealership-1024x684.jpeg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:paragraph {"align":"center","fontSize":"large"} -->
<p class="has-text-align-center has-large-font-size"></p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->