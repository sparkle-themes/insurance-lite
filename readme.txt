=== Insurance Lite ===
Contributors: sparklewpthemes
Tags: one-column, two-columns, right-sidebar, left-sidebar, custom-header, custom-background, custom-menu, translation-ready, featured-images, theme-options, custom-logo, e-commerce, footer-widgets
Requires at least: 5.2
Tested up to: 6.2
Requires PHP: 7.0
Stable tag: 1.0.4
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Insurance Lite is a fantastic multipurpose advanced free WordPress theme. Insurance lite helps you to create different types of business websites (consultant, finance, agency, industries, education, fashion, health & medical, wedding, photography, gym, architecture, lawyer) and many more with the help of theme customizer features and 10+ custom elementor addons. The Insurance lite theme is also fully compatible with the latest page builder plugins (Elementor, SiteOrigin, Visual Composer) and also supports WooCommerce and some other external plugins like Jetpack, Polylang, Yoast SEO, Contact Form 7 and many more plugins. If you face any problem related to our theme, you can refer to our theme documentation or contact our friendly support team. Demo [https://demo.sparklewpthemes.com/constructionlight/insurance-lite/]

== Description ==

Insurance Lite is a fantastic multipurpose advanced free WordPress theme. Insurance lite helps you to create different types of business websites (consultant, finance, agency, industries, education, fashion, health & medical, wedding, photography, gym, architecture, lawyer) and many more with the help of theme customizer features and 10+ custom elementor addons. The Insurance lite theme is also fully compatible with the latest page builder plugins (Elementor, SiteOrigin, Visual Composer) and also supports WooCommerce and some other external plugins like Jetpack, Polylang, Yoast SEO, Contact Form 7 and many more plugins. If you face any problem related to our theme, you can refer to our theme documentation or contact our friendly support team. Demo [https://demo.sparklewpthemes.com/constructionlight/insurance-lite/]


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports WooCommerce and some other external plugins like Jetpack, Contact Form 7 and many more plugins.

= Where can I find theme demo? =

You can check our Theme features at https://sparklewpthemes.com/wordpress-themes/insurance-multipurpose-wordpress-theme/

= Where can I find theme all features ? =

You can check out details at https://sparklewpthemes.com/wordpress-themes/insurance-multipurpose-wordpress-theme/


== Translation ==

Insurance lite theme is translation ready.


== Copyright ==

Insurance Lite WordPress Theme is child theme of constructionlight, Copyright 2017 Sparkle Themes.
Insurance Lite is distributed under the terms of the GNU GPL

Insurance Lite WordPress Theme, Copyright (C) 2022 Sparkle Themes.
Insurance Lite is distributed under the terms of the GNU GPL

== Credits ==

	Images used in screenshot

	* https://pxhere.com/en/photo/1444739, License: CCO (https://pxhere.com/en/license)
	* https://pxhere.com/en/photo/1456587, License: CCO (https://pxhere.com/en/license)
	* https://pxhere.com/en/photo/611038, License: CCO (https://pxhere.com/en/license)

== Changelog ==
= 1.0.4 6th April 2023
 ** WordPress 6.2 Compatible
 ** New Patterns Added
 ** FSE Compatible

= 1.0.3 8th Nov 2022
 ** WordPress 6.1 Compatible
 ** Typography options - added

= 1.0.2.2 23rd Sep 2022
 ** Header Contact Info CSS Issue Fixed
 ** Blog Layout Added
 ** Service Section Link Issue Fixed

= 1.0.2.1 23rd Aug 2022
 ** Header Title Color Issue - Fixed

= 1.0.2 22nd Aug 2022
 ** Slider Font Issue - Fixed
 ** Starter Content - Updated


= 1.0.1 31st July 2022
 ** RTL Compatible

= 1.0.0 26th July 2022 =
 ** Initial submit theme on wordpress.org trac.